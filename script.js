let btn = document.querySelector(".btn");
let inputs = document.querySelectorAll("input");
let icons = document.querySelectorAll("i");

for (let i = 0; i < icons.length; i++) { 

    icons[i].addEventListener('click',
        () => {
            icons[i].classList.toggle("fa-eye");
            icons[i].classList.toggle("fa-eye-slash");

            if (icons[i].classList.contains("fa-eye-slash")) {
                inputs[i].type = "text";
            }
            else { 
                inputs[i].type = "password";
            }
        }
    )
}

inputs.forEach(
    (elem) => { 
        elem.oninput = () => { 
            if (document.querySelector(".message") !== null) { 
                    document.querySelector(".message").remove();
            }
            if (document.querySelector(".message2")!==null) { 
                document.querySelector(".message2").remove();
            }
        }
    }
);


btn.onclick = (e) => {
    e.preventDefault();
    let message = document.createElement('div');
    message.style.width = "200px";
    message.style.height = "50px";
    message.style.top = `10%`;
    message.style.left = `5%`;
    message.style.backgroundColor = "#fafafa";
    message.style.border = "1px solid black";
    message.style.position = "absolute";
    message.style.display = "flex";
    message.style.alignItems = "center";
    message.style.justifyContent = "center";
    message.style.borderRadius = "3px";
    message.textContent = "You are welcome";
    message.style.cursor = "pointer";
    message.classList.add("message");

    let message2 = document.createElement('p');
    message2.style.color = "red";
    message2.textContent = "Нужно ввести одинаковые значения";
    message2.classList.add("message2");
    
    if (inputs[0].value === inputs[1].value &&
        inputs[0].value !== "" &&
        document.querySelector(".message") === null) {
        document.body.append(message);
    }
    else if (inputs[0].value !== inputs[1].value &&
        document.querySelector(".message2") === null) { 
        inputs[1].after(message2);
        }

        message.onclick = () => { 
            message.remove();
        }
}

